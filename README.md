# Spine Detection Tutorial
本專案用於 「使用人體姿態檢測模型進行脊椎檢測」教案，[課程四 inference](https://drive.google.com/file/d/1GTE3UE--ihbE-_p7sp8zSY-b5s4WTC_u/view?usp=share_link)，計算人體頭部傾斜角度。

## Getting Started
## 下載權重
[權重下載連結](https://drive.google.com/file/d/1nsTTQHMzjte8RSfGzOPJIF6nGmITxHDW/view?usp=share_link) 

## 圖片位置
本專案於 Google Colab 中執行，讀取圖片可分為：

1.於本機端上傳至暫存區
```
from google.colab import files
image_spine = files.upload() 
img_path = list(image_spine.keys())
```
2.使用範例圖檔，資料架構為
```
root_repo  
└───test_img  
    └───|─001.jpg  
        |─002.jpg  
        └─003.jpg  
```
## 執行檢測
將側身圖片代入 [simple_baseline_spine_inference](https://gitlab.com/prophet.ai.inc/spine-detection-tutorial/-/blob/main/simple_baseline_spine_inference.py)
```
#本機端圖片檢測
spine_joints,spine_img = inference(img_path[-1])
result = spine_img.copy()
```
```
#使用範例檔圖片
spine_joints,spine_img = inference(dir_path+test_image)
result = spine_img.copy()
```
## 結果
![](https://i.imgur.com/78bP3or.png)
